import decimal
import sys
import time
from decimal import Decimal

import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

import RetrievalModelsMatrix as models
import collectionloaders
import simpleparser as parser

# Configurations
np.set_printoptions(threshold=sys.maxsize)
decimal.getcontext().prec = 4


def frange(start, stop, step):
    i = start
    while i < stop:
        yield float(i)
        i = Decimal(i) + Decimal(step)


def erange(start, end, mul):
    while start < end:
        yield start
        start *= mul


### 1. Load the corpus
cranfield = collectionloaders.CranfieldTestBed()

### 2. Parse the corpus, Tokenize, stem and remove stop words
vectorizer = CountVectorizer()
vectorizer_gram = CountVectorizer(ngram_range=(1, 2), token_pattern=r'\b\w+\b', min_df=1, stop_words={'the', 'is'})

corpus = parser.stemCorpus(cranfield.corpus_cranfield['abstract'])

### 3. Create the model, Compute the term frequencies matrix and the model statistics
tf_cranfield = vectorizer.fit_transform(corpus).toarray()
tf_gram_cranfield = vectorizer_gram.fit_transform(corpus).toarray()

# Best values for parameters
lmd = 228
lmjm = 0.4

model_names = ['vsm', 'lmd', 'lmjm', 'rm3']

# Create a array of all tests with varying parameters
tests = ['vsm_uni', 'vsm_bi']

total_len = 0
for c in corpus:
    total_len = total_len + len(c)
corpus_size = int(total_len / len(corpus))

lower = corpus_size - corpus_size * 0.7
upper = corpus_size + corpus_size * 0.7

for u in erange(lower, upper + 1, 1.2):
    tests.append('lmd_' + str(int(u)))

for y in frange(0, 1.01, 0.1):
    tests.append('lmjm_' + str(y))

for m in ['lmjm', 'lmd']:
    for alpha in frange(0, 1.01, 0.1):
        for docs in range(5, 20):
            for words in range(10, 40, 10):
                tests.append('rm3_' + str(alpha) + "_" + str(docs) + "_" + str(words) + "_" + m)

### 4. Run the queries over the corpus
results = {}

print("starting")

# Got through each test
for test in tests:
    parts = test.split("_")
    test_name = parts[0]
    start = time.time()

    if test_name == model_names[0]:
        if parts[1] == 'uni':
            model = models.RetrievalModelsMatrix(tf_cranfield, vectorizer, 0, 0, 0, 0, 0)
        else:
            model = models.RetrievalModelsMatrix(tf_gram_cranfield, vectorizer_gram, 0, 0, 0, 0, 0)
    elif test_name == model_names[1]:
        u = int(parts[1])
        model = models.RetrievalModelsMatrix(tf_cranfield, vectorizer, u, 0, 0, 0, 0)
    elif test_name == model_names[2]:
        y = float(parts[1])
        model = models.RetrievalModelsMatrix(tf_cranfield, vectorizer, 0, y, 0, 0, 0)
    elif test_name == model_names[3]:
        a = float(parts[1])
        t = int(parts[2])
        w = int(parts[3])
        model = models.RetrievalModelsMatrix(tf_cranfield, vectorizer, lmd, lmjm, a, t, w)
    else:
        print("ERROR : no test available")

    # Parse the query and compute the document scores
    i = 1
    map_model = 0
    precision_model = []
    for query in cranfield.queries:
        if test_name == model_names[0]:
            scores = model.score_vsm(parser.stemSentence(query))
        elif test_name == model_names[1]:
            scores = model.score_lmd(parser.stemSentence(query))
        elif test_name == model_names[2]:
            scores = model.score_lmjm(parser.stemSentence(query))
        elif test_name == model_names[3]:
            scores = model.score_rm3(parser.stemSentence(query), parts[4])
        else:
            print("ERROR : no test available")

        # Do the evaluation
        [average_precision, precision, recall, p10] = cranfield.eval(scores, i)
        map_model = map_model + average_precision
        precision_model.append(precision)
        i = i + 1

    end = time.time()
    map_model = map_model / cranfield.num_queries
    results[test] = [map_model, precision_model]
    print(f'{test}\t\t: {round(results[test][0], 6)} : {round(end - start, 3)}s')

tops = {}

# Select top results
for res in results:
    parts = res.split("_")
    test_name = parts[0]
    if test_name not in tops:
        tops[test_name] = [res, results[res]]
    elif results[res] > tops[test_name][1]:
        tops[test_name] = [res, results[res]]

print("TOP RESULTS")
for t in tops:
    print(f'{t} -> {tops[t][0]} : {tops[t][1][0]}')
