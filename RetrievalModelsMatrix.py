import numpy as np


class RetrievalModelsMatrix:

    # 0.01-0.99 np.linspace(0.01-0.99, 10)-> array 10 points
    def __init__(self, tf, vectorizer, lmd_u, lmjm_y, rm3_alpha, rm3_doc_count, rm3_word_count):
        # print(f'u:{lmd_u}, y:{lmjm_y},a:{rm3_alpha},d:{rm3_doc_count},w:{rm3_word_count}')
        # print(np.argwhere(sizeDoc < 1))

        self.vectorizer = vectorizer
        self.tf = tf

        # VSM statistics
        # How many times the term appears in the collection
        self.term_coll_freq = np.sum(tf, axis=0)

        # Size of docs -> how many words
        self.docLen = np.sum(tf, axis=1)

        # In how many docs the term appears -> != 0 -> put in binary
        self.term_doc_freq = np.sum(tf != 0, axis=0)

        # rarity of term
        # 0 -> term appears in every document
        # 0-1 -> more than half the collection
        # 1+ -> less the half the collection
        # log of (number of documents in corpus divided by frequency of terms)
        # gives an array of terms with the corresponding rarity [1,2,3]
        self.idf = np.log(np.size(tf, axis=0) / self.term_doc_freq)

        # removing extra stop-words
        self.idf = (self.idf > 0.2) * self.idf

        # final term weight, matrix of number of documents lines by number of terms columns
        self.tfidf = np.array(tf * self.idf)

        # normalize size of vectors of each document, array of number of documents lenght
        self.docNorms = np.sqrt(np.sum(np.power(self.tfidf, 2), axis=1))

        # LM statistics
        numbTimesTermDoc = tf

        # smothing, 2 docs have lenght 0
        sizeDoc = self.docLen.reshape(1, -1).T + 0.01
        self.pMd = numbTimesTermDoc / sizeDoc

        numbTimesTermCol = np.sum(tf, 0)
        numbTermsCol = np.sum(tf)
        pMc = numbTimesTermCol / numbTermsCol

        # LMD statistics
        self.lmd = ((tf + (lmd_u * pMc)) / (lmd_u + sizeDoc))

        # LMJM statistics
        self.lmjm_y = lmjm_y
        self.fqt = lmjm_y * self.pMd + (1 - lmjm_y) * pMc

        self.logFQT = np.log(self.fqt)
        self.logLMD = np.log(self.lmd)

        # RM3 statistics
        self.alpha = rm3_alpha
        self.doc_count = rm3_doc_count
        self.word_count = rm3_word_count

    def score_vsm(self, query):
        query_vector = self.vectorizer.transform([query]).toarray()
        query_norm = np.sqrt(np.sum(np.power(query_vector, 2), axis=1))

        doc_scores = np.dot(query_vector, self.tfidf.T) / (0.0001 + self.docNorms * query_norm)

        return doc_scores

    def score_lmd(self, query):
        query_vector = self.vectorizer.transform([query]).toarray()

        doc_scores = np.prod(self.lmd ** query_vector, axis=1)

        return doc_scores

    def score_lmjm(self, query):
        query_vector = self.vectorizer.transform([query]).toarray()

        doc_scores = np.prod(self.fqt ** query_vector, axis=1)

        return doc_scores

    def score_rm3(self, query, model):
        query_vector = self.vectorizer.transform([query]).toarray()
        total_terms_query = np.sum(query_vector, axis=1)

        if model == "lmjm":
            ranks = self.score_lmjm(query)
        else:
            ranks = self.score_lmd(query)

        threshold = np.sort(ranks)[::-1][self.doc_count]
        top_docs = (ranks * (ranks > threshold)).reshape(-1, 1)

        rm1 = np.sum(top_docs * self.pMd, axis=0)

        threshold = np.sort(rm1)[::-1][self.word_count]
        rm1 = rm1 * (rm1 > threshold)
        rm1 = rm1 / np.sum(rm1)

        # normalization
        p_w_mq = query_vector / total_terms_query

        rm3 = (1 - self.alpha) * p_w_mq + self.alpha * rm1

        if model == "lmjm":
            doc_scores = np.sum(self.logFQT * rm3, axis=1)
        else:
            doc_scores = np.sum(self.logLMD * rm3, axis=1)

        return doc_scores
